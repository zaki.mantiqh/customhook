import React from "react";
import Myprops from "./comps/Myprops";
import Mytext from "./comps/Mytext";
import Mycompany from "./comps/Mycompany";
import Mychool from "./comps/Mychool";
import useUpdateLogger from "./comps/logging/useUpdateLogger";
import useLocalStorage from "./comps/storageComps/useLocalStorage";
import ThemeProvider from "./comps/storageComps/ThemeProvider";
import Navbar from "./comps/inputComps/Navbar";
import "./App.css";

var prevScrollpos = window.pageYOffset;

window.onscroll = function () {
  var currentScrollPos = window.pageYOffset;

  if (prevScrollpos > currentScrollPos) {
    document.getElementById("nav-scroll").style.top = "0px";
    document.getElementById("nav-scroll").style.transform =
      "perspective(0px) rotateX(0deg) scaleX(1)";
  } else {
    document.getElementById("nav-scroll").style.top = "-100px";
    document.getElementById("nav-scroll").style.transform =
      "perspective(400px) rotateX(45deg) scaleX(0.8)";
    document.getElementById("nav-scroll").style.transition =
      "all 1.1s cubic-bezier(1,-0.73, 0, 1.02)";
  }
  prevScrollpos = currentScrollPos;
};

function App() {
  const [myName, setMyName] = useLocalStorage("name", "");
  const [myText, setMyText] = useLocalStorage("text", "");
  const [myCompany, setMyCompany] = useLocalStorage("company", "");
  const [mySchool, setMySchool] = useLocalStorage("school", "");
  useUpdateLogger(myName);
  useUpdateLogger(myText);
  //   const [myText, setMyText] = useLocalStorage('name', () => '');
  return (
    <ThemeProvider>
      <div className='App'>
        <Navbar />
        <Myprops myName={myName} setMyName={setMyName} />
        <Mytext myText={myText} setMyText={setMyText} />
        <Mycompany myCompany={myCompany} setMyCompany={setMyCompany} />
        <Mychool mySchool={mySchool} setMySchool={setMySchool} />
      </div>
    </ThemeProvider>
  );
}

export default App;
