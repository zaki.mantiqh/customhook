import React from "react";
import TextComp from "./inputComps/TextComp";

function Mycompany({ myCompany, setMyCompany }) {
  return (
    <section>
      <div>
        <TextComp componentValue={myCompany} setComponentValue={setMyCompany} />
      </div>
      <div>Got the name prop -&gt; company = {myCompany}</div>
    </section>
  );
}

export default Mycompany;
