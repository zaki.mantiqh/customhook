import React from "react";
import TextComp from "./inputComps/TextComp";
import { useTheme } from "../comps/storageComps/ThemeProvider";

function Mychool({ mySchool, setMySchool }) {
  const theme = useTheme();
  return (
    <section className={`${theme ? "light" : "dark"}`}>
      <div>
        <TextComp componentValue={mySchool} setComponentValue={setMySchool} />
      </div>
      <div>Got the name prop -&gt; school = {mySchool}</div>
    </section>
  );
}

export default Mychool;
