import React from "react";
import { useTheme, useThemeUpdate } from "./../storageComps/ThemeProvider";

function ThemeButton() {
  const theme = useTheme();
  const toggleTheme = useThemeUpdate();
  return (
    <div>
      <input
        type='button'
        className={`${theme ? "light" : "dark"}`}
        value='Change Theme'
        onClick={() => toggleTheme()}
      />
    </div>
  );
}

export default ThemeButton;
