import React from "react";

function TextComp({ componentValue, setComponentValue }) {
  return (
    <div>
      <input
        type='text'
        onChange={(e) => setComponentValue(e.target.value)}
        value={componentValue}
      />
    </div>
  );
}

export default TextComp;
