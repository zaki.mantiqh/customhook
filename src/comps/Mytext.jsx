import React from "react";
import TextComp from "./inputComps/TextComp";

function Mytext({ myText, setMyText }) {
  return (
    <section>
      <div>
        <TextComp componentValue={myText} setComponentValue={setMyText} />
      </div>
      <div>Got the name prop -&gt; Text = {myText}</div>
    </section>
  );
}

export default Mytext;
