import React from "react";
import TextComp from "./inputComps/TextComp";
import ThemeButton from "./inputComps/ThemeButton";

function Myprops({ myName, setMyName }) {
  return (
    <section>
      <div>
        <ThemeButton />
        <TextComp componentValue={myName} setComponentValue={setMyName} />
      </div>
      <div>Got the name prop -&gt; name = {myName}</div>
    </section>
  );
}

export default Myprops;
