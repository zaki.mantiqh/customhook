import React from "react";

function useUpdateLogger(value) {
  React.useEffect(() => {
    console.log("The value changed to: ", value);
  }, [value]);
}

export default useUpdateLogger;
